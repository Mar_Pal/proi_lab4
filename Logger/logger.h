#pragma once
#include <vector>
#include <chrono>
#include <string>
//z4. Klasa Logger
//Zaimplementowac klase Logger, bedaca kolekcja ktora moze przechowywac wiadomosci
//ronych typow bazujacych na interfejsie IMessage w tym typow definiowanych przez uzytkownika.Z problemem zwiazana jest jego priorytet(np.Info, Trace, Error, itp.).
//Logger ma zapisywac takze informacje o czasie wstawienia wiadomosci do kolekcji(wykorzystac std::chrono),
//umozliwiac przegladanie zapisanych komunikatow oraz ich wypisywanie na dowolny strumien wyjsciowy.Nalezy zaimplementowac odpowiedni iterator,
//ktory umozliwiaja np.poruszanie sie po wiadomosciach o okreslonym priorytecie, czy przeglad wiadomosci w okreslonym oknie czasowym.
template<typename T>
class Logger
{
	private:
		std::vector < T > obiekty;
		friend void operator<<(std::ostream& os, const Logger<T>& l);
		//friend operator>>(std::istream& is, const Logger<T>& l);
	public:
		Logger();
		void log();
};

enum Priorytet {
        info = 0,
        trace,
        error,
        undefined
    };

class Imessage
{
	protected:
	std::string komunikat;
	time_t czas;
	enum Priorytet priorytet;
	public:
	virtual std::string msg() = 0;
	Imessage();
};
class Info
	: public Imessage
{
	public:
	Info();

};
class Trace
	: public Imessage
{
	Trace();
};
class Error
	: public Imessage
{
	Error();
};


template <typename T>
void operator<<(std::ostream& os, const Logger<T>& l);
//template<typename T>
//void operator>>(std::istream& is, const Logger<T>& l)