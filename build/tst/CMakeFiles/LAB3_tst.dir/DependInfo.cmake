# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/mnt/d/Workspace/PROI_projekt/projekt/klasa_przeszkoda/tst/Test.cpp" "/mnt/d/Workspace/PROI_projekt/projekt/klasa_przeszkoda/build/tst/CMakeFiles/LAB3_tst.dir/Test.cpp.o"
  "/mnt/d/Workspace/PROI_projekt/projekt/klasa_przeszkoda/tst/main.cpp" "/mnt/d/Workspace/PROI_projekt/projekt/klasa_przeszkoda/build/tst/CMakeFiles/LAB3_tst.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../klasa_przeszkoda"
  "../lib/googletest/googletest/include"
  "../lib/googletest/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/mnt/d/Workspace/PROI_projekt/projekt/klasa_przeszkoda/build/klasa_przeszkoda/CMakeFiles/LAB3_lib.dir/DependInfo.cmake"
  "/mnt/d/Workspace/PROI_projekt/projekt/klasa_przeszkoda/build/lib/googletest/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
